#include <string>

#define CATCH_CONFIG_MAIN
#include "catch2/catch.hpp"


TEST_CASE( "simple" )
{
    std::cout << "roam_test" << std::endl;
    std::string toRev = "Hello";
    REQUIRE( toRev == "Hello" );
}
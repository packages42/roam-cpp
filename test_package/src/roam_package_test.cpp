#include <roam/roam.h>
#include <iostream>

using namespace roam;
using namespace std;

void property_test(){
    cout << "property test" << endl;
    roam::Property name_property("Name",roam::ValueType::VT_STRING,std::string("test"));
    cout << name_property << endl;
    cout << endl;
}
void parameter_test(){
    cout << "parameter test" << endl;
    roam::Parameter name_parameter("name", roam::ValueType::VT_STRING,std::string("test"));
    cout << name_parameter << endl;
    cout << endl;
}

int main() {
    
    try{
        parameter_test();
        Tree tree(Branch(1,"Application","application"));

        Property rank("Rank",ValueType::VT_INT32,(int)(5));
        rank.id = 1;
        tree.root.properties.push_back(rank);
        tree.root.set_property_value("Rank",(int)6);
        Property rank2 = tree.root.get_property("Rank").value();
        cout << rank2 << endl;

        Method new_(1,"new");
        cout << new_ << endl;

        Method open(1,"open",Parameter("filename",ValueType::VT_STRING));
        tree.root.methods.push_back(open);

        Method open2 = tree.root.get_method("open").value();
        cout << "open2.return_parameter_type " << open2.return_parameter.type << endl;
        cout << open2 << endl;
        open2.set_parameter("filename",string("C:/test.json"));
        cout << "filename " << open2.get_value<string>("filename") << endl;

        std::string exception_what = "";
        try{
            open2.get_value<string>("");
        }
        catch(const std::runtime_error& e){
            exception_what = e.what();
        }
        if(exception_what != "parameter '' was not found for method void open(string filename)")
        {
            cout << exception_what << endl;
            throw std::runtime_error("the expected exception message was not generated");
        }
        
        Branch project(2,"Project","project");

        tree.root.children.push_back(project);

        CollectArgs args;
        args.id = 1;
        args.depth = 1;

        cout << "types" << endl;
        for(auto& type : tree.root.types()){
            cout << "  " << type << endl;
        }

        project = tree.root.descendants_of_type("Project")[0];

        cout << "sizeof bool         " << sizeof(bool) << endl;
        cout << "sizeof short        " << sizeof(short) << endl;
        cout << "sizeof long         " << sizeof(long) << endl;
        cout << "sizeof long long    " << sizeof(long long) << endl;

        Parameter p1("id",ValueType::VT_UINT64);
        cout << "Parameter p1 " << p1 << endl;
        Parameter p2 = p1;
        cout << "Parameter p2 " << p2 << endl;
        Method create(1,Parameter("id",ValueType::VT_UINT64),"create",Parameter("name",ValueType::VT_STRING));
        cout << create << endl;
        Method create_copy = create;
        cout << create << endl;
    }
    catch(exception& e){
        cout << e.what() << endl;
    }
}

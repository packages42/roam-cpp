#include <iostream>
#include "roam/roam.h"

using namespace std;
using namespace roam;

Parameter::Parameter() : type(ValueType::VT_NONE) { value_=get_default_value(ValueType::VT_NONE);}
Parameter::Parameter(const char* name,ValueType type):name(name),type(type){value_=get_default_value(type);}
Parameter::Parameter(const char* name, ValueType type, std::any value) :name(name), type(type) {
	set_value(value);
}
Parameter::Parameter(const Parameter& other) : name(other.name), type(other.type), value_(other.value_) {}
std::any Parameter::get_value() const {
	return value_;
}
void Parameter::set_value(std::any value) {
	if (type == ValueType::VT_NONE) {
		throw std::runtime_error("the value of parameter type VT_NONE may not be set");
	}
	if (!value.has_value()) {
		throw std::runtime_error("the value of parameter may not be set to a std::any that does not have a value");
	}
	if (value.type() == typeid(const char*) && type == ValueType::VT_STRING) {
		value_ = value;
	}
	else {
		if (value.type() != get_type_info(type)) {

			std::ostringstream os;
			os << "parameter of type " << type << " may not be set to std::any of type " << value.type().name();
			throw std::runtime_error(os.str().c_str());
		}
		value_ = value;
	}
}
std::string Parameter::get_value_string() const {
	return roam::get_value_string(type, value_);
}

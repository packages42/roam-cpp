#include "roam/roam.h"

using namespace std;
using namespace roam;

bool CollectArgs::is_type_match(const char* type) const{
    string stype(type);
    if(allow_partial_matches){
        for(auto& p : include_types){
            if(stype.find(p) != string::npos){return true;}
        }
        for(auto& p : exclude_types){
            if(stype.find(p) != string::npos){return false;}
        }
    }
    else{
        for(auto& p : include_types){
            if(stype == p){return true;}
        }
        for(auto& p : exclude_types){
            if(stype == p){return false;}
        }
    }
    return true;
}
#include <iostream>
#include "roam/roam.h"

using namespace std;
using namespace roam;

Tree::Tree(){}
Tree::Tree(Branch root) :root(root){}
optional<Branch> Tree::get(GetArgs args) const{
    optional<Branch> option_src_branch = find_branch(args.id);
    if(option_src_branch.has_value()){
        Branch& src_branch = option_src_branch.value();
        if(args.is_type_match(src_branch.type.c_str())){
            Branch branch;
            branch.set_tree(src_branch.get_tree());
            branch.id = src_branch.id;
            branch.name = src_branch.name;
            branch.type = src_branch.type;
            if(args.properties){
                for(auto& p : src_branch.properties){
                    branch.properties.push_back(p);
                }
            }
            if(args.methods){
                for(auto& m : src_branch.methods){
                    branch.methods.push_back(m);
                }
            }
            if(args.depth > 0){
                for(auto& cb : src_branch.children){
                    GetArgs child_get_args = args;
                    child_get_args.id = cb.id;
                    child_get_args.depth = args.depth-1;
                    optional<Branch> ocb = get(child_get_args);
                    if(ocb.has_value()){
                        branch.children.push_back(ocb.value());
                    }
                }
            }
            return optional<Branch>(branch);
        }
    }
    return optional<Branch>();
}
vector<Property> Tree::set(vector<Property> properties){
    vector<Property> results;
    return results;
}
Parameter Tree::invoke(const Method& method){
    Parameter p = method.return_parameter;;
    if (method.is_bound()) {
        p.set_value(method.iinvoke().invoke(method.get_arguments()));
    }
    return p;
}
optional<Branch> Tree::find_branch(unsigned long long id) const{
    if(root.id == id){
        return optional<Branch>(root);
    }
    return optional<Branch>();
}
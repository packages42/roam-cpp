#include <iostream>
#include "roam/roam.h"

using namespace std;
using namespace roam;

Property::Property(){}
Property::Property(const Property& other) : id(other.id), name(other.name),type(other.type),value_(other.value_){
    values_ = other.values_;
}
Property::Property(const char* name, ValueType type) : name(name), type(type) {
	set_value(get_default_value(type));
}
Property::Property(const char* name,ValueType type,std::any value) : name(name),type(type){
	set_value(value);
}
std::any Property::get_value() const {
	return value_;
}
void Property::set_value(std::any value)
{
	if (type == ValueType::VT_NONE) {
		throw std::runtime_error("the value of property type VT_NONE may not be set");
	}
	if (!value.has_value()) {
		throw std::runtime_error("the value of property may not be set to a std::any that does not have a value");
	}
	if (value.type() != get_type_info(type)) {
		std::ostringstream os;
		os << "property " << name << " of type " << type << " may not be set to std::any of type " << value.type().name();
		throw std::runtime_error(os.str().c_str());
	}
	value_ = value;
}
std::vector<std::any> Property::get_values() const {
	return values_;
}
void Property::set_values(std::vector<std::any> values) {
	for (auto& a : values) {
		if (type == ValueType::VT_NONE) {
			throw std::runtime_error("the value of property type VT_NONE may not be set");
		}
		if (!a.has_value()) {
			throw std::runtime_error("the value of property may not be set to a std::any that does not have a value");
		}
		if (a.type() != get_type_info(type)) {
			std::ostringstream os;
			os << "property of type " << type << " may not be set to std::any of type " << value_.type().name();
			throw std::runtime_error(os.str().c_str());
		}
	}
	values_ = values;
}
std::string Property::get_value_string() const {
	return roam::get_value_string(type, value_);
}
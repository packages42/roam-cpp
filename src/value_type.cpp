#include "roam/roam.h"

std::any roam::get_default_value(roam::ValueType vt){
    std::any result;
    switch(vt){
        case VT_BOOL:
            return false;
        case VT_INT32:
            return (int)0;
        case VT_INT64:
            return (long long)0;
        case VT_UINT32:
            return (unsigned int)0;
        case VT_UINT64:
            return (unsigned long long)0;
        case VT_FLOAT:
            return 0.0f;
        case VT_DOUBLE:
            return 0.0;
        case VT_STRING:
            return std::string("");
    }
    return result;
}
roam::ValueType roam::get_value_type(std::type_index ti) {
    if (ti == typeid(bool)) { return VT_BOOL; }
    if (ti == typeid(int)) { return VT_INT32; }
    if (ti == typeid(long long)) { return VT_INT64; }
    if (ti == typeid(unsigned int)) { return VT_UINT32; }
    if (ti == typeid(unsigned long long)) { return VT_UINT64; }
    if (ti == typeid(float)) { return VT_FLOAT; }
    if (ti == typeid(std::string)) { return VT_STRING; }
    if (ti == typeid(const char*)) { return VT_STRING; }
    if (ti == typeid(double)) { return VT_DOUBLE; }
    return roam::VT_NONE;
}
roam::ValueType roam::get_value_type(const std::type_info& ti) {
    if (ti == typeid(bool)) { return VT_BOOL; }
    if (ti == typeid(int)) { return VT_INT32; }
    if (ti == typeid(long long)) { return VT_INT64; }
    if (ti == typeid(unsigned int)) { return VT_UINT32; }
    if (ti == typeid(unsigned long long)) { return VT_UINT64; }
    if (ti == typeid(float)) { return VT_FLOAT; }
    if (ti == typeid(std::string)) { return VT_STRING; }
    if (ti == typeid(const char*)) { return VT_STRING; }
    if (ti == typeid(double)) { return VT_DOUBLE; }
    return roam::VT_NONE;
}
const std::type_info& roam::get_type_info(ValueType vt) {
    switch (vt) {
    case VT_BOOL:
        return typeid(bool);
    case VT_INT32:
        return typeid(int);
    case VT_INT64:
        return typeid(long long);
    case VT_UINT32:
        return typeid(unsigned int);
    case VT_UINT64:
        return typeid(unsigned long long);
    case VT_FLOAT:
        return typeid(float);
    case VT_DOUBLE:
        return typeid(double);
    case VT_STRING:
        return typeid(std::string);
    }
    throw std::runtime_error("unable to return type_info");
}
std::type_index roam::get_type_index(ValueType vt) {
    return std::type_index(get_type_info(vt));
}
std::string roam::get_type_name(ValueType vt) {
    switch (vt) {
    case ValueType::VT_BOOL:
        return "bool";
    case ValueType::VT_INT32:
        return "int32";
    case ValueType::VT_INT64:
        return "int64";
    case ValueType::VT_UINT32:
        return "uint32";
    case ValueType::VT_UINT64:
        return "uint64";
    case ValueType::VT_FLOAT:
        return "float";
    case ValueType::VT_DOUBLE:
        return "double";
    case ValueType::VT_STRING:
        return "string";
    case ValueType::VT_BYTES:
        return "bytes";
    case ValueType::VT_BRANCH:
        return "Branch";
        
    }
    return "void";
}
std::string roam::get_cpp_type_name(ValueType vt) {
    switch (vt) {
    case ValueType::VT_BOOL:
        return "bool";
    case ValueType::VT_INT32:
        return "int";
    case ValueType::VT_INT64:
        return "long";
    case ValueType::VT_UINT32:
        return "unsigned int";
    case ValueType::VT_UINT64:
        return "unsigned long long";
    case ValueType::VT_FLOAT:
        return "float";
    case ValueType::VT_DOUBLE:
        return "double";
    case ValueType::VT_STRING:
        return "string";
    case ValueType::VT_BYTES:
        return "bytes";
    case ValueType::VT_BRANCH:
        return "Branch";
        
    }
    return "void";
}
std::string roam::get_value_string(ValueType type, std::any value) {
    std::ostringstream os;
    switch (type) {
    case VT_BOOL:
        if (std::any_cast<bool>(value)) {
            os << "true";
        }
        else {
            os << "false";
        }
        break;
    case VT_INT32:
        os << std::any_cast<int>(value);
        break;
    case VT_INT64:
        os << std::any_cast<long long>(value);
        break;
    case VT_UINT32:
        os << std::any_cast<unsigned int>(value);
        break;
    case VT_UINT64:
        os << std::any_cast<unsigned long long>(value);
        break;
    case VT_FLOAT:
        os << std::any_cast<float>(value);
        break;
    case VT_DOUBLE:
        os << std::any_cast<double>(value);
        break;
    case VT_STRING:
        os << "\"" << std::any_cast<std::string>(value) << "\"";
        break;
    }
    return os.str();
}
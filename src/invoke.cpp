#include <iostream>
#include "roam/roam.h"

std::any roam::IInvoke::invoke(std::vector<std::any> args) const{
    if (args.size() != size_) {
        std::ostringstream os;
        os << "args.size was not " << size_ << " as expected";
        throw std::runtime_error(os.str().c_str());
    }
    return std::any();
}
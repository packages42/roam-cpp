#include "roam/roam.h"
#include <iomanip>

using namespace std;
using namespace roam;
ostream& operator<<(ostream& os, const ValueType& vt){
    return os << get_type_name(vt);
}
ostream& operator<<(ostream& os,const pair<ValueType,any>& pair){

      switch (pair.first) {
        case VT_NONE:
          return os << "";
          break;
        case VT_BOOL:
          return os << any_cast<bool>(pair.second);
          break;
        case VT_INT32:
          return os << any_cast<long>(pair.second);
          break;
        case VT_INT64:
          return os << any_cast<long long>(pair.second);
          break;
        case VT_UINT32:
          return os << any_cast<uint32_t>(pair.second);
          break;
        case VT_UINT64:
          return os << any_cast<uint64_t>(pair.second);
          break;
        case VT_FLOAT:
          return os << any_cast<float>(pair.second);
          break;
        case VT_DOUBLE:
          return os << any_cast<double>(pair.second);
          break;
        case VT_STRING:
          return os <<any_cast<string>(pair.second);
          break;
        case VT_BYTES:
          return os << "";
          break;
        case VT_BRANCH:
          return os << "";
          break;
      }
      return os << "";
}
ostream& operator<<(ostream& os, const Property& p){ 
    os << p.name << " " << get_cpp_type_name(p.type) << " " << p.get_value_string();
    if (p.get_values().size() > 0) {
        os << endl;
        for (auto& v : p.get_values()) {
            os << "  " << get_cpp_type_name(p.type) << " " << roam::get_value_string(p.type, v) << endl;
        }
    }
    return os;
}
ostream& operator<<(ostream& os, const Parameter& p){
    return os << p.name << " " << get_cpp_type_name(p.type) << " " << p.get_value_string();
}

ostream& operator<<(ostream& os, const Method& p){
  ostringstream returns;
  if(p.return_parameter.type == ValueType::VT_NONE){
    returns << "void";
  }
  else{
    returns << p.return_parameter.type;
  }
  ostringstream params;
  params << "(";
  int pindex =0;
  for(auto& param : p.parameters){
    if(pindex > 0){ params << ",";}
    params << param.type << " " << param.name;
    ++pindex;
  }
  params << ")";
  return os << returns.str().c_str() << " " << p.name << params.str();
}
ostream& operator<<(ostream& os, const roam::Branch& b){
    return os << make_pair(0,b);
}
ostream& operator<<(ostream& os, const pair<int,roam::Branch>& pair){
    string indent = "";
    if(pair.first > 0){
        ostringstream iss;
        iss << setw(pair.first) << " ";
        indent = iss.str();
    }

    const Branch& b = pair.second;
    os << indent << "Branch " << b.name << " (" << b.type << "," << b.id << ")" << endl;
    if(!b.properties.empty()){
      os << indent << "  " << b.properties.size() << " properties" << endl;
      for(auto& p : b.properties){
          os << indent << "    " << p << endl;
      }
    }
    if(!b.methods.empty()){
      os << indent << "  " << b.methods.size() << " methods" << endl;
      for(auto& m : b.methods){
          os << indent << "    " << m << endl;
      }
    }
    if(!b.children.empty()) {
      os << indent << "  " << b.children.size() << " children" << endl;
      for(auto& c : b.children){
          os << make_pair(pair.first+4,c);
      }
    }
    return os;
}
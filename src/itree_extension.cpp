#include <iostream>
#include "roam/roam.h"

using namespace std;
using namespace roam;

Branch ITreeExtensions::get_root(const ITree& tree) {
    optional<Branch> branch_opt = tree.get(roam::GetArgs{ tree.root_id,1,true,true });
    Branch b = Branch(branch_opt.value());
    b.set_tree(tree);
    return b;
}
Branch ITreeExtensions::get_branch(const ITree& tree,unsigned long long id, int depth, bool properties, bool methods) {
    GetArgs get_args;
    get_args.id = id;
    get_args.depth = depth;
    get_args.properties = properties;
    get_args.methods = methods;
    std::optional<Branch> branch_opt = tree.get(get_args);
    if (branch_opt.has_value()) {
        Branch branch(branch_opt.value());
        return branch;
    }
    else {
        std::ostringstream os;
        os << "Branch with id " << " was not found";
        throw std::runtime_error(os.str().c_str());
    }
}
std::any ITreeExtensions::invoke(ITree& tree, unsigned long long id, const char* name, std::vector<std::any> args) {
    roam::GetArgs ga;
    ga.id = id;
    ga.depth = 0;
    ga.properties = false;
    ga.methods = true;
    std::optional<Branch> branch_opt = tree.get(ga);
    if (branch_opt.has_value()) {
        Branch branch(branch_opt.value());
        for (auto& m : branch.methods) {
            if (m.name == name) {
                m.id = id;
                m.set_parameters(args);
                Parameter result = tree.invoke(m);
                return result.get_value();
            }
        }
    }
    else {
        std::ostringstream os;
        os << "unable to resolve id " << id;
        throw std::runtime_error(os.str().c_str());
    }
    std::ostringstream os;
    os << "method '" << name << "' not found";
    throw std::runtime_error(os.str().c_str());
}
std::any ITreeExtensions::invoke(ITree& tree, unsigned long long id, const char* name) {
    return invoke(tree,id,name,{});
}
#include <iostream>
#include "roam/roam.h"
#include "roam/Invoke.hpp"

using namespace std;
using namespace roam;

Method::Method() : id(0),name("") { return_parameter = Parameter("void",roam::ValueType::VT_NONE);}
Method::Method(unsigned long long id,const char* name):id(id),name(name){}
Method::Method(unsigned long long id,const char* name,Parameter p1) : Method(id,name) {
    parameters.push_back(p1);
}
Method::Method(unsigned long long id,const char* name,Parameter p1,Parameter p2) : Method(id,name,p1){
    parameters.push_back(p2);
}
Method::Method(unsigned long long id,const char* name,Parameter p1,Parameter p2,Parameter p3) : Method(id,name,p1,p2){
    parameters.push_back(p3);
}
Method::Method(unsigned long long id,Parameter return_param,const char* name) : Method(id,name){
    return_parameter = return_param;
}
Method::Method(unsigned long long id,Parameter return_parameter,const char* name,Parameter p1) 
 : Method(id,return_parameter,name){
    parameters.push_back(p1);
}
Method::Method(unsigned long long id,Parameter return_parameter,const char* name,Parameter p1,Parameter p2) 
 : Method(id,return_parameter,name,p1){
    parameters.push_back(p2);
}
Method::Method(unsigned long long id,Parameter return_parameter,const char* name,Parameter p1,Parameter p2,Parameter p3) 
 : Method(id,return_parameter,name,p1,p2){
    parameters.push_back(p3);
}
Method::Method(const char* name,IInvoke* invoke) : name(name), pinvoke_(invoke) {
    // set parameters
    int index = 1;
    for (auto& param_type_info : invoke->parameter_types()) {
        std::ostringstream os;
        os << "p" << index;
        Parameter parameter(os.str().c_str(), get_value_type(param_type_info));
        parameters.push_back(parameter);
        ++index;
    }
    // set return parameter
    return_parameter.type = get_value_type(invoke->return_type());
}
optional<Parameter> Method::get_parameter(const char* name) const{
    for(auto& p : parameters){
        if(p.name == name){
            return optional<Parameter>(p);
        }
    }
    return optional<Parameter>();
}
std::vector<std::any> Method::get_arguments() const {
    std::vector<std::any> args;
    for (auto& p : parameters) {
        args.push_back(p.get_value());
    }
    return args;
}
void Method::set_parameter(const char* name,const any& value) {
    for(auto& p : parameters){
        if(p.name == name){
            p.set_value(value);
        }
    }
}
void Method::set_parameter(const char* name,const char* value){
    this->set_parameter(name,std::string(value));
}
void Method::set_parameters(std::vector<std::any> args) {
    if (parameters.size() != args.size()) {
        std::ostringstream os;
        os << "parameters.size = " << parameters.size() << " but " << args.size() << " arguments were passed";
        throw std::runtime_error(os.str().c_str());
    }
    for (int i = 0; i < args.size(); ++i) {
        parameters[i].set_value(args[i]);
    }
}
std::string Method::to_s() const{
    ostringstream os;
    os << *this;
    return os.str();
}
std::any Method::get_any_value(const char* name) const {
    std::optional<Parameter> opt = get_parameter(name);
    if(opt.has_value()){
        return opt.value().get_value();
    }
    else{
        std::ostringstream os;
        os << "parameter '" << name << "' was not found for method " << to_s();
        throw std::runtime_error(os.str().c_str());
    }
}
std::any Method::invoke(std::vector<std::any> args) {
    if (pinvoke_.get() == static_cast<roam::IInvoke*>(0)) {
        throw std::runtime_error("no IInvoke binding, unable to invoke");
    }
    else {
        return pinvoke_->invoke(args);
    }
}
bool Method::is_bound() const { return pinvoke_.get() == static_cast<roam::IInvoke*>(0) ? false : true; }
const IInvoke& Method::iinvoke() const{
    return *(pinvoke_.get());
}
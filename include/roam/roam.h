#pragma once

#include <string>
#include <any>
#include <vector>
#include <optional>
#include <sstream>
#include <set>
#include <functional>
#include <memory>
#include <typeindex>

namespace roam {
    class ITree;
}

namespace roam{
  enum ValueType {
    VT_NONE = 0,
    VT_BOOL = 1,
    VT_INT32 = 2,
    VT_INT64 = 3,
    VT_UINT32 = 4,
    VT_UINT64 = 5,
    VT_FLOAT = 6,
    VT_DOUBLE = 7,
    VT_STRING = 8,
    VT_BYTES = 9,
    VT_BRANCH = 10
  };
  std::any get_default_value(ValueType vt);
  ValueType get_value_type(std::type_index ti);
  ValueType get_value_type(const std::type_info& ti);
  const std::type_info& get_type_info(ValueType vt);
  std::type_index get_type_index(ValueType vt);
  std::string get_type_name(ValueType vt);
  std::string get_cpp_type_name(ValueType vt);
  std::string get_value_string(ValueType type,std::any value);
}

namespace roam{
  struct Property {
    public:
      Property();
      Property(const char* name, ValueType type);
      Property(const char* name,ValueType type,std::any value);
      Property(const Property& other);
      unsigned long long id;
      std::string name;
      ValueType type;
      std::any get_value() const;
      void set_value(std::any value);
      std::vector<std::any> get_values() const;
      void set_values(std::vector<std::any> values);
      std::string get_value_string() const;
  private:
      std::any value_;
      std::vector<std::any> values_;
      std::vector<std::string> display_values;
  };
  
}

namespace roam{
  struct Parameter {
    public:
      Parameter();
      Parameter(const char* name,ValueType type);
      Parameter(const char* name,ValueType type,std::any value);
      Parameter(const Parameter& other);
    public:
      std::string name;
      ValueType type;

      std::any get_value() const;
      void set_value(std::any value);
      std::string get_value_string() const;
  private:
      std::any value_;
  };
  
}

namespace roam {
    struct IInvoke {
    public:
        IInvoke(int size) : size_(size) {}
        virtual std::any invoke(std::vector<std::any> args) const;
        virtual IInvoke* clone() = 0;
        virtual const std::type_info& return_type() const = 0;
        virtual std::vector<std::type_index> parameter_types() const = 0;
    protected:
        int size_;
    };
}
namespace roam{
  struct Method {
    public:
       Method();
       Method(unsigned long long id,const char* name);
       Method(unsigned long long id,const char* name,Parameter p1);
       Method(unsigned long long id,const char* name,Parameter p1,Parameter p2);
       Method(unsigned long long id,const char* name,Parameter p1,Parameter p2,Parameter p3);
       Method(unsigned long long id,Parameter return_parameter,const char* name);
       Method(unsigned long long id,Parameter return_parameter,const char* name,Parameter p1);
       Method(unsigned long long id,Parameter return_parameter,const char* name,Parameter p1,Parameter p2);
       Method(unsigned long long id,Parameter return_parameter,const char* name,Parameter p1,Parameter p2,Parameter p3);
       Method(const char* name,IInvoke* invoke);
    public:
      unsigned long long id;
      std::string name;
      std::string type;
      std::vector<Parameter> parameters;
      Parameter return_parameter;
      std::optional<Parameter> get_parameter(const char* name) const;
      std::vector<std::any> get_arguments() const;
      std::any get_any_value(const char* name) const;
      template<typename T>
      T get_value(const char* name) const {
        return std::any_cast<T>(get_any_value(name));
      }
      void set_parameter(const char* name,const std::any& value);
      void set_parameter(const char* name,const char* value);
      void set_parameters(std::vector<std::any> args);
      std::string to_s() const;
      virtual std::any invoke(std::vector<std::any> args);
      bool is_bound() const;
      const IInvoke& iinvoke() const;
  private:
      std::shared_ptr<IInvoke> pinvoke_;
  };

}
namespace roam{
  struct Branch {
    public:
        Branch();
        Branch(const char* type);
        Branch(const char* type,const char* name);
        Branch(unsigned long long id, const char* type, const char* name);
        Branch(const Branch& other);
        unsigned long long id;
        std::string name;
        std::string type;
        std::vector<Property> properties;
        std::vector<Method> methods;
        std::vector<Branch> children;
        std::optional<Property> get_property(const char* name) const;
        void set_property_value(const char* name,const std::any& value);
        std::optional<Method> get_method(const char* name) const;
        std::vector<Branch> descendants() const;
        std::vector<Branch> descendants_of_type(const char* type) const;
        std::vector<Branch> flatten() const;
        std::set<std::string> types() const;
        void set_tree(const ITree& tree);
        ITree& get_tree();
        const ITree& get_tree() const;
      private:
        ITree* _ptree;
  };
}

namespace roam{
  struct GetArgs{
    public:
      unsigned long long id;
      // a depth of 0 indicates that no descendant branches are collected
      // a depth of 1 indicates that direct children are to be collected
      // a depth of 2 indicates that children and grandchildren are to be collected
      unsigned int depth; 
      bool properties;
      bool methods;
      std::vector<std::string> exclude_types;
      std::vector<std::string> include_types;
      bool allow_partial_matches;
      bool is_type_match(const char* type) const;
  };
}

namespace roam{
  struct CollectArgs{
    public:
      unsigned long long id;
      // a depth of 0 indicates that no descendant branches are collected
      // a depth of 1 indicates that direct children are to be collected
      // a depth of 2 indicates that children and grandchildren are to be collected
      unsigned int depth; 
      bool properties;
      bool methods;
      std::vector<std::string> exclude_types;
      std::vector<std::string> include_types;
      bool allow_partial_matches;
      bool is_type_match(const char* type) const;
  };
}
namespace roam{
  class ITree{
    public:
      ITree();
      ITree(unsigned long long root_id,const char* name,const char* description);
      std::string name;
      std::string description;
      unsigned long long root_id;
      virtual std::optional<Branch> get(GetArgs args) const = 0;
      virtual std::vector<Property> set(std::vector<Property> properties) = 0;
      virtual Parameter invoke(const Method& method) = 0;
  };
}

namespace roam {
    class ITreeExtensions {
    public:
        static Branch get_root(const ITree& tree);
        static Branch get_branch(const ITree& tree, unsigned long long id, int depth = 0, bool properties = false, bool methods = false);
        static std::any invoke(ITree& tree,unsigned long long id, const char* name, std::vector<std::any> args);
        static std::any invoke(ITree& tree,unsigned long long id, const char* name);
    };
}

namespace roam{
  class Tree : public ITree {
    public:
      Tree();
      Tree(Branch root);
      Branch root;
      virtual std::optional<Branch> get(GetArgs args) const override;;
      virtual std::vector<Property> set(std::vector<Property> properties) override;
      virtual Parameter invoke(const Method& method) override;
    private:
      std::optional<Branch> find_branch(unsigned long long id) const;
  };
}



std::ostream& operator<<(std::ostream& os, const roam::ValueType& vt);
std::ostream& operator<<(std::ostream& os,const std::pair<roam::ValueType,std::any>& v);
std::ostream& operator<<(std::ostream& os, const roam::Property& p);
std::ostream& operator<<(std::ostream& os, const roam::Parameter& p);
std::ostream& operator<<(std::ostream& os, const roam::Method& p);
std::ostream& operator<<(std::ostream& os, const roam::Branch& p);
std::ostream& operator<<(std::ostream& os, const std::pair<int,roam::Branch>& pair);

namespace roam {
    template<typename TResult>
    struct Invoke0 : public IInvoke {
    public:
        Invoke0(std::function<TResult()> fn) : IInvoke(0), fn_(fn) {}
        std::any invoke(std::vector<std::any> args) const override {
            IInvoke::invoke(args);
            return std::any(fn_());
        }
        IInvoke* clone() override { return new Invoke0(fn_); }
        const std::type_info& return_type() const { return typeid(TResult); }
        std::vector<std::type_index> parameter_types() const override { return {}; }
    private:
        std::function<TResult()> fn_;
    };
    template<typename TResult, typename T1>
    struct Invoke1 : public IInvoke {
    public:
        Invoke1(std::function<TResult(T1)> fn) : IInvoke(1), fn_(fn) {}
        std::any invoke(std::vector<std::any> args) const override {
            IInvoke::invoke(args);
            return std::any(fn_(std::any_cast<T1>(args[0])));
        }
        IInvoke* clone() override { return new Invoke1(fn_); }
        const std::type_info& return_type() const { return typeid(TResult); }
        std::vector<std::type_index> parameter_types() const override { return { typeid(T1) }; }
    private:
        std::function<TResult(T1)> fn_;
    };
    template<typename TResult, typename T1, typename T2>
    struct Invoke2 : public IInvoke {
    public:
        Invoke2(std::function<TResult(T1, T2)> fn) : IInvoke(2), fn_(fn) {}
        std::any invoke(std::vector<std::any> args) const override {
            IInvoke::invoke(args);
            return std::any(fn_(std::any_cast<T1>(args[0]), std::any_cast<T2>(args[1])));
        }
        IInvoke* clone() override { return new Invoke2(fn_); }
        const std::type_info& return_type() const override { return typeid(TResult); }
        std::vector<std::type_index> parameter_types() const { return { typeid(T1),typeid(T2) }; }
    private:
        std::function<TResult(T1, T2)> fn_;
    };
    template<typename TResult, typename T1, typename T2, typename T3>
    struct Invoke3 : public IInvoke {
    public:
        Invoke3(std::function<TResult(T1, T2, T3)> fn) : IInvoke(3), fn_(fn) {}
        std::any invoke(std::vector<std::any> args) const override {
            IInvoke::invoke(args);
            return std::any(fn_(std::any_cast<T1>(args[0]), std::any_cast<T2>(args[1]), std::any_cast<T3>(args[2])));
        }
        IInvoke* clone() override { return new Invoke3(fn_); }
        const std::type_info& return_type() const { return typeid(TResult); }
        std::vector<std::type_index> parameter_types() const override { return { typeid(T1),typeid(T2),typeid(T3) }; }
    private:
        std::function<TResult(T1, T2, T3)> fn_;
    };
    template<typename TResult, typename T1, typename T2, typename T3, typename T4>
    struct Invoke4 : public IInvoke {
    public:
        Invoke4(std::function<TResult(T1, T2, T3, T4)> fn) : IInvoke(4), fn_(fn) {}
        std::any invoke(std::vector<std::any> args)const override {
            IInvoke::invoke(args);
            return std::any(fn_(std::any_cast<T1>(args[0]),
                std::any_cast<T2>(args[1]),
                std::any_cast<T3>(args[2]),
                std::any_cast<T4>(args[3])));
        }
        IInvoke* clone() override { return new Invoke4(fn_); }
        const std::type_info& return_type() const { return typeid(TResult); }
        std::vector<std::type_index> parameter_types() const override { return { typeid(T1),typeid(T2),typeid(T3),typeid(T4) }; }
    private:
        std::function<TResult(T1, T2, T3, T4)> fn_;
    };
    template<typename TResult, typename T1, typename T2, typename T3, typename T4, typename T5>
    struct Invoke5 : public IInvoke {
    public:
        Invoke5(std::function<TResult(T1, T2, T3, T4, T5)> fn) : IInvoke(5), fn_(fn) {}
        std::any invoke(std::vector<std::any> args)const override {
            IInvoke::invoke(args);
            return std::any(fn_(std::any_cast<T1>(args[0]),
                std::any_cast<T2>(args[1]),
                std::any_cast<T3>(args[2]),
                std::any_cast<T4>(args[3]),
                std::any_cast<T5>(args[4])));
        }
        IInvoke* clone() override { return new Invoke5(fn_); }
        const std::type_info& return_type() const { return typeid(TResult); }
        std::vector<std::type_index> parameter_types() const override { return { typeid(T1),typeid(T2),typeid(T3),typeid(T4),typeid(T5)}; }
    private:
        std::function<TResult(T1, T2, T3, T4, T5)> fn_;
    };
    template<typename TResult, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6>
    struct Invoke6 : public IInvoke {
    public:
        Invoke6(std::function<TResult(T1, T2, T3, T4, T5, T6)> fn) : IInvoke(6), fn_(fn) {}
        std::any invoke(std::vector<std::any> args) const override {
            IInvoke::invoke(args);
            return std::any(fn_(std::any_cast<T1>(args[0]),
                std::any_cast<T2>(args[1]),
                std::any_cast<T3>(args[2]),
                std::any_cast<T4>(args[3]),
                std::any_cast<T5>(args[4]),
                std::any_cast<T6>(args[5])));
        }
        IInvoke* clone() override { return new Invoke6(fn_); }
        const std::type_info& return_type() const { return typeid(TResult); }
        std::vector<std::type_index> parameter_types() const override { return { typeid(T1),typeid(T2),typeid(T3),typeid(T4),typeid(T5),typeid(T6)}; }
    private:
        std::function<TResult(T1, T2, T3, T4, T5, T6)> fn_;
    };
    template<typename TResult, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7>
    struct Invoke7 : public IInvoke {
    public:
        Invoke7(std::function<TResult(T1, T2, T3, T4, T5, T6, T7)> fn) : IInvoke(7), fn_(fn) {}
        std::any invoke(std::vector<std::any> args) const override {
            IInvoke::invoke(args);
            return std::any(fn_(std::any_cast<T1>(args[0]),
                std::any_cast<T2>(args[1]),
                std::any_cast<T3>(args[2]),
                std::any_cast<T4>(args[3]),
                std::any_cast<T5>(args[4]),
                std::any_cast<T6>(args[5]),
                std::any_cast<T7>(args[6])));
        }
        IInvoke* clone() override { return new Invoke7(fn_); }
        const std::type_info& return_type() const { return typeid(TResult); }
        std::vector<std::type_index> parameter_types() const override { return { typeid(T1),typeid(T2),typeid(T3),typeid(T4),typeid(T5),typeid(T6),typeid(T7)}; }
    private:
        std::function<TResult(T1, T2, T3, T4, T5, T6, T7)> fn_;
    };
}
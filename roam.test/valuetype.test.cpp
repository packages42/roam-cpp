#include "roam/roam.h"
#include <catch2/benchmark/catch_benchmark_all.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_session.hpp>
#include <iostream>

using namespace std;

/*
VT_NONE = 0,
    VT_BOOL = 1,
    VT_INT32 = 2,
    VT_INT64 = 3,
    VT_UINT32 = 4,
    VT_UINT64 = 5,
    VT_FLOAT = 6,
    VT_DOUBLE = 7,
    VT_STRING = 8,
    VT_BYTES = 9,
    VT_BRANCH = 10*/

TEST_CASE("ValueType") {
	cout << "ValueType " << roam::ValueType::VT_NONE << endl;
	cout << "ValueType " << roam::ValueType::VT_BOOL << " " << get_cpp_type_name(roam::ValueType::VT_BOOL) << endl;
    REQUIRE(get_type_info(roam::ValueType::VT_BOOL) == typeid(bool));
    REQUIRE(get_type_index(roam::ValueType::VT_BOOL) == typeid(bool));
    cout << "ValueType " << roam::ValueType::VT_INT32 << endl;
    REQUIRE(get_type_info(roam::ValueType::VT_INT32) == typeid(int));
    REQUIRE(get_type_index(roam::ValueType::VT_INT32) == typeid(int));
    cout << "ValueType " << roam::ValueType::VT_INT64 << endl;
    REQUIRE(get_type_info(roam::ValueType::VT_INT64) == typeid(long long));
    REQUIRE(get_type_index(roam::ValueType::VT_INT64) == typeid(long long));
    cout << "ValueType " << roam::ValueType::VT_UINT32 << endl;
    REQUIRE(get_type_info(roam::ValueType::VT_UINT32) == typeid(unsigned int));
    REQUIRE(get_type_index(roam::ValueType::VT_UINT32) == typeid(unsigned int));
    cout << "ValueType " << roam::ValueType::VT_UINT64 << endl;
    REQUIRE(get_type_info(roam::ValueType::VT_UINT64) == typeid(unsigned long long));
    REQUIRE(get_type_index(roam::ValueType::VT_UINT64) == typeid(unsigned long long));
    cout << "ValueType " << roam::ValueType::VT_FLOAT << endl;
    REQUIRE(get_type_info(roam::ValueType::VT_FLOAT) == typeid(float));
    REQUIRE(get_type_index(roam::ValueType::VT_FLOAT) == typeid(float));
    cout << "ValueType " << roam::ValueType::VT_DOUBLE << endl;
    REQUIRE(get_type_info(roam::ValueType::VT_DOUBLE) == typeid(double));
    REQUIRE(get_type_index(roam::ValueType::VT_DOUBLE) == typeid(double));
    cout << "ValueType " << roam::ValueType::VT_STRING << endl;
    REQUIRE(get_type_info(roam::ValueType::VT_STRING) == typeid(std::string));
    REQUIRE(get_type_index(roam::ValueType::VT_STRING) == typeid(std::string));
}
// roam.test.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include "roam/roam.h"
#include <catch2/benchmark/catch_benchmark_all.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_session.hpp>
#include <iostream>
#include "testtree.h"
using namespace std;

int main(int argc, char* argv[])
{
    try {
		return Catch::Session().run(argc, argv);
    }
	catch (const std::exception& e) {
		cout << "=======================================================================" << endl;
		cout << "std::exception raised" << endl;
		cout << "=======================================================================" << endl;
		cout << e.what() << endl;
		cout << "=======================================================================" << endl;
		return 1;
	}
	catch (...) {
		cout << "=======================================================================" << endl;
		cout << "unknown exception, consider improving the exception reporting" << endl;
		cout << "=======================================================================" << endl;
		return 1;
	}
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file

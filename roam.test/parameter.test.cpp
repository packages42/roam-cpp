#include "roam/roam.h"
#include <catch2/benchmark/catch_benchmark_all.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_session.hpp>
#include <iostream>

using namespace std;

TEST_CASE("Parameters") {
	roam::Parameter default_parameter;
	cout << "default_parameter " << default_parameter << endl;
	REQUIRE(default_parameter.type == roam::VT_NONE);

	roam::Parameter bool_parameter("bool_parameter",roam::VT_BOOL);
	cout << bool_parameter << endl;
	REQUIRE(bool_parameter.type == roam::VT_BOOL);
	REQUIRE(std::any_cast<bool>(bool_parameter.get_value()) == false);

	roam::Parameter int32_parameter("int32_parameter", roam::VT_INT32);
	cout << int32_parameter << endl;
	REQUIRE(int32_parameter.type == roam::VT_INT32);
	REQUIRE(std::any_cast<int>(int32_parameter.get_value()) == 0);

	roam::Parameter int64_parameter("int64_parameter", roam::VT_INT64);
	cout << int64_parameter << endl;
	REQUIRE(int64_parameter.type == roam::VT_INT64);
	REQUIRE(std::any_cast<long long>(int64_parameter.get_value()) == 0);

	roam::Parameter uint32_parameter("uint32_parameter", roam::VT_UINT32);
	cout << uint32_parameter << endl;
	REQUIRE(uint32_parameter.type == roam::VT_UINT32);
	REQUIRE(std::any_cast<unsigned int>(uint32_parameter.get_value()) == 0);

	roam::Parameter uint64_parameter("uint64_parameter", roam::VT_UINT64);
	cout << uint64_parameter << endl;
	REQUIRE(uint64_parameter.type == roam::VT_UINT64);
	REQUIRE(std::any_cast<unsigned long long>(uint64_parameter.get_value()) == 0);

	roam::Parameter float_parameter("float_parameter", roam::VT_FLOAT);
	cout << float_parameter << endl;
	REQUIRE(float_parameter.type == roam::VT_FLOAT);
	REQUIRE(std::any_cast<float>(float_parameter.get_value()) == 0.f);

	roam::Parameter double_parameter("double_parameter", roam::VT_DOUBLE);
	cout << double_parameter << endl;
	REQUIRE(double_parameter.type == roam::VT_DOUBLE);
	REQUIRE(std::any_cast<double>(double_parameter.get_value()) == 0.0);

	roam::Parameter string_parameter("string_parameter", roam::VT_STRING);
	cout << string_parameter << endl;
	REQUIRE(string_parameter.type == roam::VT_STRING);
	REQUIRE(std::any_cast<std::string>(string_parameter.get_value()) == "");
}
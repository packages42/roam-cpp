#include "testtree.h"

TestTree::TestTree() : roam::Tree() {
    name = "test";
    description = "a test tree";
    root = roam::Branch(1001, "Branch", "root");
    root_id = root.id;
	root.set_tree(*this);
	root.methods.push_back(roam::Method("add",
		new roam::Invoke2<int, int, int>(std::bind(TestTree::add, std::placeholders::_1, std::placeholders::_2))));
	root.methods.push_back(roam::Method("concat",
		new roam::Invoke2<std::string, const char*, const char*>(std::bind(TestTree::concat, std::placeholders::_1, std::placeholders::_2))));
}

int TestTree::add(int a, int b) { return a + b; }
std::string TestTree::concat(const char* a, const char* b) {
	std::ostringstream os;
	os << a << b;
	return os.str();
}

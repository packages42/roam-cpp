#include "roam/roam.h"
#include <catch2/benchmark/catch_benchmark_all.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_session.hpp>
#include <iostream>

using namespace std;

int method_test_score() { return 42; }
int method_test_add(int a, int b) { return a + b; }
std::string method_test_concat(const char* a, const char* b ){
	return std::string(a) + b;
}

TEST_CASE("Method no parameters") {
	cout << "Method no parameters" << endl;
	roam::Method score("score",new roam::Invoke0<int>(std::bind(method_test_score)));
	REQUIRE(std::any_cast<int>(score.invoke({})) == 42);
}
TEST_CASE("Method two parameters") {
	cout << "Method two parameters" << endl;
	roam::Method add("add",new roam::Invoke2<int, int, int>(std::bind(method_test_add, std::placeholders::_1, std::placeholders::_2)));
	REQUIRE(add.iinvoke().parameter_types().size() == 2);
	REQUIRE(add.iinvoke().parameter_types()[0] == typeid(int));
	REQUIRE(add.iinvoke().parameter_types()[1] == typeid(int));
	REQUIRE(add.parameters.size() == 2);
	REQUIRE(add.parameters[0].type == roam::ValueType::VT_INT32);
	REQUIRE(add.parameters[1].type == roam::ValueType::VT_INT32);
	REQUIRE(add.return_parameter.type == roam::ValueType::VT_INT32);
	REQUIRE(std::any_cast<int>(add.invoke({ std::any{1},std::any{2} })) == 3);

	roam::Method add2(add);
	REQUIRE(std::any_cast<int>(add2.invoke({ std::any{1},std::any{2} })) == 3);

	roam::Method concat("concat", new roam::Invoke2<std::string, const char*, const char*>(std::bind(method_test_concat, std::placeholders::_1, std::placeholders::_2)));
	REQUIRE(concat.iinvoke().parameter_types().size() == 2);
	REQUIRE(concat.iinvoke().parameter_types()[0] == typeid(const char*));
	REQUIRE(concat.iinvoke().parameter_types()[1] == typeid(const char*));
	REQUIRE(concat.parameters.size() == 2);
	REQUIRE(concat.parameters[0].type == roam::ValueType::VT_STRING);
	REQUIRE(concat.parameters[1].type == roam::ValueType::VT_STRING);
	REQUIRE(concat.return_parameter.type == roam::ValueType::VT_STRING);
	REQUIRE(std::any_cast<std::string>(concat.invoke({ std::any{"a"},std::any{"b"}})) == "ab");
}

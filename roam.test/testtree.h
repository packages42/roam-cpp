#pragma once
#include "roam/roam.h"
class TestTree : public roam::Tree {
public:
    TestTree();
    static int add(int a, int b);
    static std::string concat(const char* a, const char* b);
};
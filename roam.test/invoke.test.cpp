#include "roam/roam.h"
#include <catch2/benchmark/catch_benchmark_all.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_session.hpp>
#include <iostream>

using namespace std;

int invoke_test_score() { return 42; }
int invoke_test_add(int a, int b) { return a + b; }
std::string invoke_test_concat2(const char* a, const char* b) {
	std::ostringstream os;
	os << a << b;
	return os.str();
}
std::string invoke_test_concat3(const char* a, const char* b,const char* c) {
	std::ostringstream os;
	os << a << b << c;
	return os.str();
}
std::string invoke_test_concat4(const char* a, const char* b, const char* c,const char* d) {
	std::ostringstream os;
	os << a << b << c << d;
	return os.str();
}
std::string invoke_test_concat5(const char* a, const char* b, const char* c, const char* d,const char* e) {
	std::ostringstream os;
	os << a << b << c << d << e;
	return os.str();
}
std::string invoke_test_concat6(const char* a, const char* b, const char* c, const char* d, const char* e,const char* f) {
	std::ostringstream os;
	os << a << b << c << d << e << f;
	return os.str();
}
std::string invoke_test_concat7(const char* a, const char* b, const char* c, const char* d, const char* e, const char* f,const char* g) {
	std::ostringstream os;
	os << a << b << c << d << e << f << g;
	return os.str();
}

TEST_CASE("Invoke Test No Parameters") {
	std::unique_ptr<roam::IInvoke> pinvoke(new roam::Invoke0<int>(std::bind(invoke_test_score)));
	REQUIRE(std::any_cast<int>(pinvoke->invoke({})) == 42);
	std::unique_ptr<roam::IInvoke> pinvoke2(pinvoke->clone());
	REQUIRE(std::any_cast<int>(pinvoke2->invoke({})) == 42);
}

TEST_CASE("Invoke Test Two int Parameters") {
	std::unique_ptr<roam::IInvoke> pinvoke(new roam::Invoke2<int,int,int>(std::bind(invoke_test_add,std::placeholders::_1,std::placeholders::_2)));
	REQUIRE(std::any_cast<int>(pinvoke->invoke({std::any(1),std::any(2)})) == 3);
}

TEST_CASE("Invoke Test Two const char* Parameters") {
	std::unique_ptr<roam::IInvoke> pinvoke(new roam::Invoke2<std::string, const char*, const char*>(std::bind(invoke_test_concat2, std::placeholders::_1, std::placeholders::_2)));
	REQUIRE(std::any_cast<std::string>(pinvoke->invoke({ std::any("a"),std::any("b")})) == "ab");
	std::unique_ptr<roam::IInvoke> pinvoke2(pinvoke->clone());
	REQUIRE(std::any_cast<std::string>(pinvoke2->invoke({ std::any("a"),std::any("b") })) == "ab");
}

TEST_CASE("Invoke Test Three const char* Parameters") {
	std::unique_ptr<roam::IInvoke> pinvoke(new roam::Invoke3<std::string, const char*, const char*,const char*>(std::bind(invoke_test_concat3, std::placeholders::_1, std::placeholders::_2,std::placeholders::_3)));
	REQUIRE(std::any_cast<std::string>(pinvoke->invoke({ std::any("a"),std::any("b"),std::any("c")})) == "abc");
}

TEST_CASE("Invoke Test Four const char* Parameters") {
	std::unique_ptr<roam::IInvoke> pinvoke(new roam::Invoke4<std::string, const char*, const char*,const char*,const char*>(std::bind(invoke_test_concat4, std::placeholders::_1, std::placeholders::_2,std::placeholders::_3,std::placeholders::_4)));
	REQUIRE(std::any_cast<std::string>(pinvoke->invoke({ std::any("a"),std::any("b"),std::any("c"),std::any("d") })) == "abcd");
}

TEST_CASE("Invoke Test Five const char* Parameters") {
	std::unique_ptr<roam::IInvoke> pinvoke(new roam::Invoke5<std::string, const char*, const char*, const char*, const char*,const char*>(
		std::bind(invoke_test_concat5, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4,std::placeholders::_5)));
	REQUIRE(std::any_cast<std::string>(pinvoke->invoke({ std::any("a"),std::any("b"),std::any("c"),std::any("d"),std::any("e")})) == "abcde");
}

TEST_CASE("Invoke Test Six const char* Parameters") {
	std::unique_ptr<roam::IInvoke> pinvoke(new roam::Invoke6<std::string, const char*, const char*, const char*, const char*, const char*,const char*>(
		std::bind(invoke_test_concat6, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5,std::placeholders::_6)));
	REQUIRE(std::any_cast<std::string>(pinvoke->invoke({ std::any("a"),std::any("b"),std::any("c"),std::any("d"),std::any("e"),std::any("f")})) == "abcdef");
}

TEST_CASE("Invoke Test Seven const char* Parameters") {
	std::unique_ptr<roam::IInvoke> pinvoke(new roam::Invoke7<std::string, const char*, const char*, const char*, const char*, const char*, const char*, const char*>(
		std::bind(invoke_test_concat7, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5, std::placeholders::_6,std::placeholders::_7)));
	REQUIRE(std::any_cast<std::string>(pinvoke->invoke({ std::any("a"),std::any("b"),std::any("c"),std::any("d"),std::any("e"),std::any("f"),std::any("g")})) == "abcdefg");
}

#include "roam/roam.h"
#include <catch2/benchmark/catch_benchmark_all.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_session.hpp>
#include <iostream>

using namespace std;

TEST_CASE("Properties") {
	cout << roam::Property("bool_property",roam::VT_BOOL) << endl;
	cout << roam::Property("int32_property", roam::VT_INT32) << endl;
	cout << roam::Property("int64_property", roam::VT_INT64) << endl;
	cout << roam::Property("uint32_property", roam::VT_UINT32) << endl;
	cout << roam::Property("uint64_property", roam::VT_UINT64) << endl;
	cout << roam::Property("float_property", roam::VT_FLOAT) << endl;
	cout << roam::Property("double_property", roam::VT_DOUBLE) << endl;
	cout << roam::Property("string_property", roam::VT_STRING) << endl;
}
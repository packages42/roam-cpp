#include "roam/roam.h"
#include <catch2/catch_test_macros.hpp>

#include <iostream>
#include "testtree.h"

using namespace roam;
using namespace std;

TEST_CASE("Tree Identity") {
    cout << "Tree Identity" << endl;
    TestTree test_tree;
    ITree& tree = test_tree;
    REQUIRE(tree.root_id == 1001);
    REQUIRE(tree.name == "test");
    REQUIRE(tree.description == "a test tree");

    roam::Branch root = roam::ITreeExtensions::get_root(tree);
    REQUIRE(root.id == tree.root_id);
    REQUIRE(root.name == "root");
    REQUIRE(root.type == "Branch");
    REQUIRE(root.get_tree().root_id == tree.root_id);
    REQUIRE(root.methods.size() == 2);

    roam::Branch root2 = roam::ITreeExtensions::get_branch(tree, root.id,0,false,true);
    REQUIRE(root2.id == tree.root_id);
    REQUIRE(root2.name == "root");
    REQUIRE(root2.type == "Branch");
    REQUIRE(root2.get_tree().root_id == tree.root_id);
    REQUIRE(root2.methods.size() == 2);
}
TEST_CASE("Tree Methods") {
	cout << "Tree Methods" << endl;
    TestTree test_tree;
    ITree& tree = test_tree;
    REQUIRE(std::any_cast<int>(roam::ITreeExtensions::invoke(tree, 1001, "add", { std::any(1),std::any(2) })) == 3);
    REQUIRE(std::any_cast<std::string>(roam::ITreeExtensions::invoke(tree, 1001, "concat", { std::any("a"),std::any("b")})) == "ab");
}

require "raykit"

PROJECT.set_version(Raykit::Version::detect_from_file("conanfile.py", /version[\s]+=[\s]+"([\d\.]+)/, false))

task :clean do
  puts "  removing roam packages from the local cache"
  run "conan remove roam/* --force"
end

task :build do
  start_task :build

  try "rufo ." if (!PROJECT.read_only?)

  if Gem.win_platform?
    #run("conan config install https://gitlab.com/cpp-lib/roam.git -sf=profiles -tf=profiles")
    run("conan create . --profile win_vs2022_x64_static_release")
  else
    run("conan create .")
    Dir.chdir("test") do
      Dir.mkdir("build") if !Dir.exists?("build")
      PROJECT.run("conan install . --install-folder build")
      Dir.chdir("build") do
        PROJECT.run("cmake ..")
        PROJECT.run("cmake --build .")
      end
    end
  end
end

task :test => [:build] do
  start_task :test
  if Gem.win_platform?
    run("test_package/build/Release/roam_package_test.exe").details
  end

  Dir.chdir("roam.test") do
    run("conan install . --install-folder build --build=missing -pr=win_vs2022_x86_dynamic_release")
    run("conan install . --install-folder build --build=missing -pr=win_vs2022_x86_dynamic_debug")
    run("conan install . --install-folder build --build=missing -pr=win_vs2022_x64_dynamic_release")
    run("conan install . --install-folder build --build=missing -pr=win_vs2022_x64_dynamic_debug")
  end

  msbuild_path = Raykit::MsBuild::msbuild_path()
  run("\"#{msbuild_path}/msbuild\" roam.test.sln /p:Configuration=Release /p:Platform=x64")
  run("roam.test/x64/Release/roam.test.exe").details
end

task :default => [:test, :integrate, :tag, :push] do
  puts "  completed #{PROJECT.name}.#{PROJECT.version} in #{PROJECT.elapsed}"
end

task :cmake do
  start_task :cmake
  run("git clean -dXf")
  run("conan install . --install-folder build --build=missing -pr=win_vs2022_x64_static_release")
  Dir.chdir("build") do
    run("cmake -A Win32 ..")
    run("cmake --build . --config Release")
  end
end

task :make do
  start_task :make
  run("conan remove roam/* --force").details
  roam_version = "0.0.3"
  if (`conan search roam/#{roam_version}`.include?("no packages matching"))
    puts "  roam/#{roam_version} recipe not found"
    Raykit::Git::Repository::make("https://gitlab.com/cpp-lib/roam.git", "v#{roam_version}", "rake build")
  else
    puts "  roam/#{roam_version} recipe found"
  end
end

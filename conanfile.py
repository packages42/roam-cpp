from conans import ConanFile
from conan.tools.cmake import CMakeToolchain, CMake
from conan.tools.layout import cmake_layout

class RoamConan(ConanFile):
    name = "roam"
    version = "0.0.41"

    # Optional metadata
    homepage = "https://gitlab.com/cpp-lib/roam"
    license = "MIT"
    author = "Lou Parslow lou.parslow@gmail.com"
    url = "https://gitlab.com/cpp-lib/roam.git"
    description = "a generalized tree data structure"
    topics = ("tree", "data", "structure")

    # Binary configuration
    settings = "os", "compiler", "build_type", "arch"
    options = { "shared": [True,False], "fPIC": [True, False]}
    default_options = {"shared":False, "fPIC": True}

    # Sources are located in the same place as this recipe, copy them to the recipe
    exports_sources = "CMakeLists.txt", "src/*","include/*"

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def layout(self):
        cmake_layout(self)

    def generate(self):
        tc = CMakeToolchain(self)
        tc.generate()

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        
        cmake = CMake(self)
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["roam"]
        self.cpp_info.bindirs = ["bin"]
